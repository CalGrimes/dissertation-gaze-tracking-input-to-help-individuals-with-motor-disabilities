import PySimpleGUI as sg
import pyautogui
from keyboard import keyboard
import cv2
from gaze_tracking import GazeTracking

gaze = GazeTracking()
webcam = cv2.VideoCapture(0)

class GUI(object):
    def __init__(self):
        sz=(125,0)
        col1 = [[sg.Text('Assistance System', size=(20,1), font='Arial 24')],
                  [sg.Input(size=(17, 1), key='input1', do_not_clear=True)],
                  #[sg.InputText(size=(17, 1), key='input2', do_not_clear=True)],
                  #[sg.Button('on-screen keyboard', key='keyboard')],
                  [sg.Button('enter', key='enter', size=(10,5))],
                  [sg.Button('close', key='close', size=(10,5))],
                  [sg.Text(size=sz)],
                  [sg.Button('General Help', key='general', size=(10,5)), sg.Button('Food/Drink', key='food', size=(10,5)), sg.Button('Sit up', key='sit', size=(10,5)), sg.Button('Emergency!', key='emergency', size=(10,5))]]
        col2 = [[sg.Image(filename='', key='image')]]
        layout = [[sg.Column(col1, element_justification='c'), sg.VSeperator(pad=(0,0)), sg.Column(col2, element_justification='c')]]
        
#        layout = [[sg.Text('Enter Text', size=(20,1), font='Arial 24')],
#                  [sg.Input(size=(17, 1), key='input1', do_not_clear=True)],
#                  [sg.InputText(size=(17, 1), key='input2', do_not_clear=True)],
#                  [sg.Button('on-screen keyboard', key='keyboard')],
#                  [sg.Button('close', key='close')]]
        wh = pyautogui.size()
        self.mainWindow = sg.Window('On-screen test',
                                    grab_anywhere=True,
                                    no_titlebar=False,
                                    size=(wh[0],wh[1]),
                                    ).Layout(layout).Finalize()
        location = self.mainWindow.CurrentLocation()
        location = location[0], location[1]+400
        self.keyboard = keyboard(location)
        self.focus = None

    def run(self):
        while True:
            cur_focus = self.mainWindow.FindElementWithFocus()
            if cur_focus is not None:
                self.focus = cur_focus
            event, values = self.mainWindow.Read(timeout=200, timeout_key='timeout')
            
            if self.focus is not None:
                self.keyboard.update(self.focus)
            if event == 'keyboard':
                self.keyboard.togglevis()
            if event == 'general':
                self.mainWindow['input1'].update("")
                sg.popup_timed('Help is coming', keep_on_top=True)
            if event == 'food':
                self.mainWindow['input1'].update("")
                sg.popup_timed('Food/Drink help is coming', keep_on_top=True)
            if event == 'sit':
                self.mainWindow['input1'].update("")
                sg.popup_timed('Sitting you up', keep_on_top=True)
            if event == 'emergency':
                self.mainWindow['input1'].update("")
                sg.popup_timed('Assistance is coming ASAP', keep_on_top=True, title='EMERGENCY', background_color='#FF0000', font=("Arial", 14))
            if event == 'enter':
                self.mainWindow['input1'].update("")
                sg.popup_timed('Text Entered', keep_on_top=True)
            elif event == 'close' or event is None:
                break
                
        
        self.keyboard.close()
        self.mainWindow.Close()
    
    def returnTrack():
        _, frame = webcam.read()
        frame = cv2.flip(frame, flipCode = -1)
        # We send this frame to GazeTracking to analyze it
        gaze.refresh(frame)

        frame = gaze.annotated_frame()
        text = ""


        if gaze.is_left():
                text = "Looking left"
        if gaze.is_right():
                text = "Looking right"
        if gaze.is_center():
            if gaze.is_top():
                text = "Looking up"
            elif gaze.is_bottom():
                text = "Looking down"
            else:
                text = "Looking center"
        if gaze.is_blinking():
            text = "Blinking"

        cv2.putText(frame, text, (90, 60), cv2.FONT_HERSHEY_DUPLEX, 1.6, (147, 58, 31), 2)

        left_pupil = gaze.pupil_left_coords()
        right_pupil = gaze.pupil_right_coords()
        cv2.putText(frame, "Left pupil:  " + str(left_pupil), (90, 130), cv2.FONT_HERSHEY_DUPLEX, 0.9, (147, 58, 31), 1)
        cv2.putText(frame, "Right pupil: " + str(right_pupil), (90, 165), cv2.FONT_HERSHEY_DUPLEX, 0.9, (147, 58, 31), 1)
        return frame   