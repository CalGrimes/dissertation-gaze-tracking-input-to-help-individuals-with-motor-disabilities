import PySimpleGUI as sg
import cv2
import numpy as np
import cv2
from gaze_tracking import GazeTracking
import pyautogui

class CameraDisplay(object):
    
    def __init__(self):
        self.cap = cv2.VideoCapture(0)
        self.gaze = GazeTracking()
        
        
    def readFrames(self):
        ret, frame = self.cap.read()
        
        self.gaze.refresh(frame)
        frame = self.gaze.annotated_frame()
        return cv2.flip(frame, flipCode = -1)
    
    def outputFrameText(self, frame, text):
        #self.gaze.refresh(frame)
        left_pupil = self.gaze.pupil_left_coords()
        right_pupil = self.gaze.pupil_right_coords()
        
        cv2.putText(frame, text, (90, 60), cv2.FONT_HERSHEY_DUPLEX, 1.6, (147, 58, 31), 2)
        cv2.putText(frame, "Left pupil:  " + str(left_pupil), (90, 130), cv2.FONT_HERSHEY_DUPLEX, 0.9, (147, 58, 31), 1)
        cv2.putText(frame, "Right pupil: " + str(right_pupil), (90, 165), cv2.FONT_HERSHEY_DUPLEX, 0.9, (147, 58, 31), 1)
        
        return frame
        
    def calibrateGaze(self):
        frame = readFrames(self)
        return cv2.imencode(".png", frame)[1].tobytes()
    
    def getGazeDirection(self):
        #self.gaze.refresh(frame)
        if self.gaze.is_top():
            if self.gaze.is_left():
                return "Looking top left"
            elif self.gaze.is_right():
                return "Looking top right"
            return "Looking up"
        if self.gaze.is_bottomDir():
            if self.gaze.is_left():
                return "Looking bottom left"
            if self.gaze.is_right():
                return "Looking bottom right"
            
        if self.gaze.is_left():
            return "Looking left"
        if self.gaze.is_right():
            return "Looking right"
        if self.gaze.is_bottom():
            return "Looking down"
        if self.gaze.is_center():
            return "Looking center"
        if self.gaze.is_blinking():
            return "Blinking"

